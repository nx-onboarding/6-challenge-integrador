# Serverless

## Ejercicio practico: integrador

### Features:

- Migrar las Lambdas (create-card | create-client) a eBased

- Agregar las funcionalidades faltantes para completar el CRUD de clientes (getAll, getById, update y delete). Teniendo en cuenta que el update modificará automáticamente el regalo de cumpleaños y la tarjeta de crédito en caso de que cambie la fecha de nacimiento. El delete se hará mediante cambio de estado.

- Además de las funcionalidades mencionadas para clientes, se agrega otro núcleo específico para compras que tendrá las siguientes características:

  - Permitirá cargar compras indicando dni de cliente. Las mismas se componen de un array de objetos que incluyen nombre del producto y precio original. Solo permitirá hacer compras con clientes activos.

  - Se agregará automáticamente a cada producto el atributo precio final, que será el resultado del precio original aplicando un descuento según la tarjeta que tenga el cliente, si es Gold será un 12% mientras que si es Classic será un 8%

  - Cada compra (que a su vez puede incluir varios productos) tendrá su propio ID y contará como un atributo el dni del cliente.

  - Cada $200 de compra (en precio final) se le otorgará al cliente 1 punto. Los mismos se acumularán en el registro del cliente.

### Deploy:

Debe de tener configurado AWS con sus credenciales

```properties
npm i -g serverless
```

```properties
serverless deploy --verbose

```

### API Gateway

- /clients GET
- /client POST
  - {dni} GET
  - {dni} PUT
  - {dni} DELETE
- /purchase POST

/purchase POST

### Body Create Client

```json
{
  "dni": "12345679000",
  "firstName": "Pedro",
  "lastName": "Suarez",
  "birthDate": "1987-12-01"
}
```

### Body Create Purchase

```json
{
  "dni": "12345679000",
  "firstName": "Pedro",
  "lastName": "Suarez",
  "birthDate": "1987-12-01"
}
```

### Remove Stack

```properties
serverless remove --verbose
```

### Nota:

El repositorio tiene configurado CI, hace el despliegue unicamente con la rama master
