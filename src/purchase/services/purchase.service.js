const dynamodb = require('ebased/service/storage/dynamo');
const { ErrorHandled } = require('ebased/util/error');
const { mapClientInfo } = require('../../helpers');

const configDB = {
  TableName: process.env.PURCHASE_TABLE,
};

const createPurchaseService = async (payload) => {
  payload = { ...payload, active: true };
  await dynamodb.putItem({
    ...configDB,
    Item: payload,
  });
};

module.exports = {
  createPurchaseService,
};
