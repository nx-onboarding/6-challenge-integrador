const { InputValidation } = require('ebased/schema/inputValidation');

class CreatePurchaseInput extends InputValidation {
  constructor(payload, meta) {
    super({
      source: meta.source,
      payload,
      type: 'PURCHASE.CREATE',
      specversion: '1.0.0',
      schema: {
        strict: true,
        id: { type: String, required: true },
        dni_client: { type: String, required: true },
        products: { type: [], required: true },
      },
    });
  }
}

module.exports = {
  CreatePurchaseInput,
};
