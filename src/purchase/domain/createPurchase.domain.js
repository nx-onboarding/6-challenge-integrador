const { ErrorHandled } = require('ebased/util/error');
const { v4: uuid } = require('uuid');

const {
  getClientByDniService,
  updateClientService,
} = require('../../client/services/client.service');
const { createPurchaseService } = require('../services/purchase.service');

const { CreatePurchaseInput } = require('../schemas/inputs/createPurchase.input');
const { discountByTypeClient } = require('../../helpers');

module.exports = async (commandPayload, commandMeta) => {
  commandPayload.id = uuid();
  const data = new CreatePurchaseInput(commandPayload, commandMeta).get();

  let clientDB = await getClientByDniService(data.dni_client);

  if (!clientDB.active) {
    throw new ErrorHandled('Customer is not active', { status: 404, code: 'CLIENT_INACTIVE' });
  }

  let { client, purchase } = discountByTypeClient(data, clientDB);

  await createPurchaseService(purchase);
  await updateClientService(client);

  try {
    return {
      statusCode: 200,
      body: JSON.stringify({
        message: 'Purchase successfully registered',
      }),
    };
  } catch (error) {
    throw new ErrorHandled(error.message || 'Internal Server Error', {
      status: 500,
      code: 'ERROR',
    });
  }
};
