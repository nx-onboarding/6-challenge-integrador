const dynamodb = require('ebased/service/storage/dynamo');

const configDB = {
  TableName: process.env.CLIENTS_TABLE,
};

const createGiftService = async (payload) => {
  const { dni, gift } = payload;

  const dbParams = {
    ...configDB,
    ExpressionAttributeNames: {
      '#G': 'gift',
    },
    ExpressionAttributeValues: {
      ':g': gift,
    },
    UpdateExpression: 'SET #G = :g',
    ReturnValues: 'UPDATED_NEW',
    Key: {
      dni,
    },
  };

  await dynamodb.updateItem(dbParams);
};

const getClientService = async (dni) => {
  const { Item } = await dynamodb.getItem({
    ...configDB,
    Key: { dni },
  });

  return Item;
};

module.exports = {
  createGiftService,
  getClientService,
};
