const { giftByBirthDate } = require('../../helpers');
const { createGiftService, getClientService } = require('../services/gift.service');
const { CreateGiftInput } = require('../schemas/inputs/createGift.input');

module.exports = async (eventPayload, eventMeta) => {
  const { Message } = eventPayload;
  let client = new CreateGiftInput(JSON.parse(Message), eventMeta).get();

  client = {
    ...client,
    gift: giftByBirthDate(client.birthDate),
  };

  let clientDb = await getClientService(client.dni);

  if (clientDb.gift === undefined || client.gift !== clientDb.gift) {
    await createGiftService(client);
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      client,
      message: 'Card successfully registered',
    }),
  };
};
