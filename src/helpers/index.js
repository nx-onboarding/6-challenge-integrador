const { ErrorHandled } = require('ebased/util/error');

const ages = {
  maxAge: 65,
  minAge: 18,
  minAgeForCard: 45,
};

const calculateAge = (date) => {
  let timeDiff = Math.abs(Date.now() - new Date(date).getTime());
  return Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
};

const validateAge = (data) => {
  const age = calculateAge(data.birthDate);
  if (age < ages['minAge'] || age > ages['maxAge']) {
    throw new ErrorHandled(`Must be between ${ages['minAge']} and ${ages['maxAge']} years old`, {
      status: 404,
      code: 'CLIENT_AGE_INVALID',
    });
  }

  return data;
};

const typeCardByAge = (birthDate) => {
  return calculateAge(birthDate) < ages['minAgeForCard'] ? 'Classic' : 'Gold';
};

const numberByLength = (length = 16) => {
  return Math.random()
    .toString()
    .substring(2, length + 2);
};

const numberRnd = (minimum, maximum) => {
  const result = Math.round(Math.random() * (maximum - minimum) + minimum);
  return result < 10 ? `0${result}` : result;
};

const giftByBirthDate = (birthday) => {
  const month = Number(birthday.split('-')[1]);
  if (3 <= month && month <= 5) {
    return 'buzo';
  }

  if (6 <= month && month <= 8) {
    return 'sweater';
  }

  if (9 <= month && month <= 11) {
    return 'camisa';
  }

  return 'remera';
};

const mapClientInfo = (data, params) => {
  Object.keys(data).forEach((key) => {
    if (key !== 'dni') {
      params.UpdateExpression += ` #${key}=:${key},`;
      params.ExpressionAttributeNames[`#${key}`] = key;
      params.ExpressionAttributeValues[`:${key}`] = data[key];
    }
  });

  params.UpdateExpression = params.UpdateExpression.slice(0, -1);

  return params;
};

const discountByTypeClient = (purchaseData, client) => {
  const TYPECARD = {
    Gold: process.env.GOLD_DISCOUNT,
    Classic: process.env.CLASSIC_DISCOUNT,
  };

  let total = 0;

  const products = purchaseData.products.map((product) => {
    const amount = product.price - product.price * TYPECARD[client.creditCard.type];
    total += amount;

    return {
      ...product,
      amount,
    };
  });

  const purchase = {
    ...purchaseData,
    products,
  };
  const points = Math.floor(total / process.env.AMOUNT_BY_POINT);
  const totalPoints = client.points ? client.points + points : points;

  client.points = totalPoints;

  return {
    purchase,
    client,
  };
};

module.exports = {
  giftByBirthDate,
  numberByLength,
  numberRnd,
  validateAge,
  typeCardByAge,
  mapClientInfo,
  discountByTypeClient,
};
