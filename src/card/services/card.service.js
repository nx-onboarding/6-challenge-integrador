const dynamodb = require('ebased/service/storage/dynamo');

const configDB = {
  TableName: process.env.CLIENTS_TABLE,
};

const createCardService = async (payload) => {
  const { dni, ccv, expirationDate, cardNumber, type } = payload;

  const dbParams = {
    ...configDB,
    ExpressionAttributeNames: {
      '#C': 'creditCard',
    },
    ExpressionAttributeValues: {
      ':c': {
        ccv,
        expirationDate,
        cardNumber,
        type,
      },
    },
    UpdateExpression: 'SET #C = :c',
    ReturnValues: 'ALL_NEW',
    Key: {
      dni,
    },
  };

  await dynamodb.updateItem(dbParams);
};

const getClientService = async (dni) => {
  const { Item } = await dynamodb.getItem({
    ...configDB,
    Key: { dni },
  });

  return Item;
};

module.exports = {
  createCardService,
  getClientService,
};
