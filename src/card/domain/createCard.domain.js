const { numberByLength, numberRnd, typeCardByAge } = require('../../helpers');
const { CreateCardInput } = require('../schemas/inputs/createCard.input');
const { createCardService, getClientService } = require('../services/card.service');

module.exports = async (eventPayload, eventMeta) => {
  const { Message } = eventPayload;
  let client = new CreateCardInput(JSON.parse(Message), eventMeta).get();

  client = {
    ...client,
    ccv: numberByLength(3),
    expirationDate: `${numberRnd(1, 12)}/${numberRnd(23, 33)}`,
    cardNumber: numberByLength(16),
    type: typeCardByAge(client.birthDate),
  };

  let clientDb = await getClientService(client.dni);

  if (clientDb.creditCard === undefined || client.type !== clientDb.creditCard.type) {
    await createCardService(client);
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      client,
      message: 'Card successfully registered',
    }),
  };
};
