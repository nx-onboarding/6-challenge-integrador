const { getClientByDniService, updateClientService } = require('../services/client.service');
const { snsUpdateClientService } = require('../services/snsUpdateClient.service');
const { UpdateClientInput } = require('../schemas/inputs/updateClient.input');
const { ErrorHandled } = require('ebased/util/error');
const { ClientUpdatedEvent } = require('../schemas/events/clientUpdated.event');

module.exports = async (commandPayload, commandMeta) => {
  const data = new UpdateClientInput(commandPayload, commandMeta).get();
  await getClientByDniService(data.dni);
  const clientDB = await getClientByDniService(data.dni);
  const client = await updateClientService(data);

  if (clientDB.birthDate !== data.birthDate) {
    await snsUpdateClientService(new ClientUpdatedEvent(commandPayload, commandMeta));
  }

  try {
    return { status: 200, body: client };
  } catch (error) {
    throw new ErrorHandled(error.message || 'Unable to Client', {
      status: 400,
      code: 'ERROR',
    });
  }
};
