const { getClientByDniService, deleteClientService } = require('../services/client.service');
const { GetClientByDniInput } = require('../schemas/inputs/getClientByDni.input');
const { ErrorHandled } = require('ebased/util/error');

module.exports = async (commandPayload, commandMeta) => {
  const { dni } = new GetClientByDniInput(commandPayload, commandMeta).get();
  await getClientByDniService(dni);
  const client = await deleteClientService(dni);

  try {
    return { status: 200, body: client };
  } catch (error) {
    throw new ErrorHandled(error.message || 'Unable to Client', {
      status: 400,
      code: 'ERROR',
    });
  }
};
