const { getClientByDniService } = require('../services/client.service');
const { GetClientByDniInput } = require('../schemas/inputs/getClientByDni.input');
const { ErrorHandled } = require('ebased/util/error');

module.exports = async (commandPayload, commandMeta) => {
  const { dni } = new GetClientByDniInput(commandPayload, commandMeta).get();
  const client = await getClientByDniService(dni);
  try {
    return { status: 200, body: client };
  } catch (error) {
    throw new ErrorHandled(error.message || 'Unable to Client', {
      status: 400,
      code: 'ERROR',
    });
  }
};
