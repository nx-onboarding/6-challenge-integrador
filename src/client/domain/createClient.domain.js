const { CreateClientInput } = require('../schemas/inputs/createClient.input');
const { ClientCreatedEvent } = require('../schemas/events/clientCreated.event');
const { createClientService, existClientService } = require('../services/client.service');
const { snsCreateClientService } = require('../services/snsCreateClient.service');
const { validateAge } = require('../../helpers');

module.exports = async (commandPayload, commandMeta) => {
  const client = new CreateClientInput(commandPayload, commandMeta).get();
  await existClientService(commandPayload);
  validateAge(commandPayload);
  await createClientService(commandPayload);
  await snsCreateClientService(new ClientCreatedEvent(commandPayload, commandMeta));

  return {
    statusCode: 200,
    body: JSON.stringify({
      client,
      message: 'Client successfully registered',
    }),
  };
};
