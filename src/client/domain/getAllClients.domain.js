const { getAllClientsService } = require('../services/client.service');
const { ErrorHandled } = require('ebased/util/error');

module.exports = async (commandPayload, commandMeta) => {
  const { Items } = await getAllClientsService();

  try {
    return { status: 200, body: Items };
  } catch (error) {
    throw new ErrorHandled(error.message || 'Unable to List Clients', {
      status: 400,
      code: 'ERROR',
    });
  }
};
