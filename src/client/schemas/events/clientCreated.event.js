const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class ClientCreatedEvent extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      meta,
      payload,
      type: 'CLIENT.CREATED',
      specversion: '1.0.0',
      schema: {
        strict: true,
        dni: { type: String, required: true },
        firstName: { type: String, required: true },
        lastName: { type: String, required: true },
        birthDate: { type: String, required: true },
      },
    });
  }
}

module.exports = {
  ClientCreatedEvent,
};
