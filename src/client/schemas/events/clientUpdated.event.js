const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class ClientUpdatedEvent extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      meta,
      payload,
      type: 'CLIENT.UPDATED',
      specversion: '1.0.0',
      schema: {
        strict: false,
        dni: { type: String, required: true },
        firstName: { type: String, required: false },
        lastName: { type: String, required: false },
        birthDate: { type: String, required: false },
      },
    });
  }
}

module.exports = {
  ClientUpdatedEvent,
};
