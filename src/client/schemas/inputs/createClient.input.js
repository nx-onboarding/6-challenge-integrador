const { InputValidation } = require('ebased/schema/inputValidation');

class CreateClientInput extends InputValidation {
  constructor(payload, meta) {
    super({
      source: meta.source,
      payload,
      type: 'CLIENT.CREATE',
      specversion: '1.0.0',
      schema: {
        strict: true,
        dni: { type: String, required: true },
        firstName: { type: String, required: true },
        lastName: { type: String, required: true },
        birthDate: { type: String, required: true },
      },
    });
  }
}

module.exports = {
  CreateClientInput,
};
