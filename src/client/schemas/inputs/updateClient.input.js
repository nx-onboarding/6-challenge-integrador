const { InputValidation } = require('ebased/schema/inputValidation');

class UpdateClientInput extends InputValidation {
  constructor(payload, meta) {
    super({
      payload,
      source: meta.source,
      specversion: '1.0.0',
      type: 'CLIENT.UPDATE',
      schema: {
        strict: false,
        dni: { type: String, required: true },
        firstName: { type: String, required: false },
        lastName: { type: String, required: false },
        birthDate: { type: String, required: false },
      },
    });
  }
}

module.exports = {
  UpdateClientInput,
};
