const { InputValidation } = require('ebased/schema/inputValidation');

class GetClientByDniInput extends InputValidation {
  constructor(payload, meta) {
    super({
      payload,
      source: meta.source,
      specversion: '1.0.0',
      type: 'CLIENT.READ',
      schema: {
        strict: false,
        dni: { type: String, required: true },
      },
    });
  }
}

module.exports = {
  GetClientByDniInput,
};
