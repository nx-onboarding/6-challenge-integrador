const sns = require('ebased/service/downstream/sns');

const snsUpdateClientService = async (clientCreateEvent) => {
  const { eventPayload, eventMeta } = clientCreateEvent.get();
  await sns.publish(
    {
      TopicArn: process.env.SNS_CLIENTS_TOPIC,
      Message: eventPayload,
    },
    eventMeta,
  );
};

module.exports = { snsUpdateClientService };
