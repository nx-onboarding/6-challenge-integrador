const dynamodb = require('ebased/service/storage/dynamo');
const { ErrorHandled } = require('ebased/util/error');
const { mapClientInfo } = require('../../helpers');


const configDB = {
  TableName: process.env.CLIENTS_TABLE,
};

const createClientService = async (payload) => {
  payload = { ...payload, active: true };
  await dynamodb.putItem({
    ...configDB,
    Item: payload,
  });
};

const existClientService = async (payload) => {
  const { Item } = await dynamodb.getItem({
    ...configDB,
    Key: { dni: payload.dni },
  });

  if (Item) {
    throw new ErrorHandled('Customer already exists', { status: 404, code: 'CLIENT_DUPLICATE' });
  }
};

const getAllClientsService = async () => {
  const clients = await dynamodb.scanTable({
    ...configDB,
  });

  try {
    return clients || [];
  } catch (error) {
    throw new ErrorHandled(error.message || 'Unable to List Clients', {
      status: 400,
      code: 'ERROR',
    });
  }
};

const getClientByDniService = async (dni) => {
  const { Item } = await dynamodb.getItem({
    ...configDB,
    Key: { dni },
  });

  if (!Item) {
    throw new ErrorHandled('Customer not exists', { status: 404, code: 'CLIENT_NOT_FOUND' });
  }
  return Item;
};

const deleteClientService = async (dni) => {
  const Item = await dynamodb.updateItem({
    ...configDB,
    Key: { dni },
    ExpressionAttributeNames: { '#active': 'active' },
    ExpressionAttributeValues: {
      ':active': false,
    },
    UpdateExpression: 'SET #active = :active',
    ReturnValues: 'ALL_NEW',
  });

  return Item;
};

const updateClientService = async (data) => {
  let params = {
    ...configDB,
    Key: { dni: data.dni },
    UpdateExpression: 'SET',
    ExpressionAttributeNames: {},
    ExpressionAttributeValues: {},
    ReturnValues: 'ALL_NEW',
  };

  params = {
    ...params,
    ...mapClientInfo(data, params),
  };

  const Item = await dynamodb.updateItem(params);

  return Item;
};

module.exports = {
  createClientService,
  existClientService,
  getAllClientsService,
  getClientByDniService,
  deleteClientService,
  updateClientService,
};
